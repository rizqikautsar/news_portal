/* Fixed menu
–––––––––––––––––––––––––––––––––––––––––––––––––– */
$(window).scroll(function () {
    var sc = $(window).scrollTop()
    if (sc > 50) {
        $("#navbar-scroll").addClass("small")
    } else {
        $("#navbar-scroll").removeClass("small")
    }
});

/* Hamburger menu
–––––––––––––––––––––––––––––––––––––––––––––––––– */
var menuButton = document.querySelector('.header-menu-button');
menuButton.addEventListener('click', menuTog);

function menuTog() {
    var headerWrapper = document.querySelector('.header-wrapper');
    var headerSpans = menuButton.querySelectorAll('span');
    this.classList.toggle('active-menu-button');
    headerWrapper.classList.toggle('active-menu');
    for (var i = 0; i < headerSpans.length; i++) {
        headerSpans[i].classList.toggle('active-span');
    }
}

/* Slick Slider
–––––––––––––––––––––––––––––––––––––––––––––––––– */
$('.featured-news').slick();
